-- Saisir ici vos instructions dml (executables a partir de grhum ou d'un user dba)
--creation de l'association DEPOS (depositaire de salle
INSERT
INTO GRHUM.association
  (
    ass_alias,
    ass_code,
    ass_id,
    ass_libelle,
    ass_locale,
    ass_racine,
    d_creation,
    d_modification,
    tas_id
  )
  (SELECT NULL,
      'DEPOS',
      GRHUM.association_seq.nextval,
      'Dépositaire de salle',
      'N',
      NULL,
      sysdate,
      sysdate,
      1
    FROM dual
  ); 