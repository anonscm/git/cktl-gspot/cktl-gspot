    /**
 * @author wanka
 */
Effect.BlindRight = function(element) {
  element = $(element);
  var elementDimensions = element.getDimensions();
  return new Effect.Scale(element, 100, Object.extend({ 
    scaleContent: false, 
    scaleY: false,
    scaleFrom: 0,
    scaleMode: {originalHeight: elementDimensions.height, originalWidth: elementDimensions.width},
    restoreAfterFinish: true,
    afterSetup: function(effect) {
      effect.element.makeClipping().setStyle({width: '0px'}).show(); 
    },  
    afterFinishInternal: function(effect) {
      effect.element.undoClipping();
    }
  }, arguments[1] || { }));
};

Effect.BlindLeft = function(element) {
  element = $(element);
  element.makeClipping();
  return new Effect.Scale(element, 0,
    Object.extend({ scaleContent: false, 
      scaleY: false, 
      restoreAfterFinish: true,
      afterFinishInternal: function(effect) {
        effect.element.hide().undoClipping();
      } 
    }, arguments[1] || { })
  );
};
var win;
/* ouvre une fenetre avec window.js*/
function openWindow( width,height,title,statusInfo,idContent){
	if (win != undefined) {
	
	}else{ 
		win = new Window({className: "dialog", width:width, height:height,resizable: true, title: title, showEffect:Effect.BlindRight, hideEffect: Effect.BlindLeft, draggable:true, wiredDrag: false,recenterAuto: false});
	}
	win.setZIndex(99999);	
	win.setContent(idContent, true, true) ;
	win.setStatusBar(statusInfo);	
	win.show();
}