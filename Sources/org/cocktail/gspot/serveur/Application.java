/*
 * Copyright Cocktail, 2001-2008
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.gspot.serveur;

import java.util.TimeZone;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlacces.server.handler.JarResourceRequestHandler;
import org.cocktail.fwkcktlajaxwebext.serveur.CocktailAjaxApplication;
import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.fwkcktlwebapp.server.init.NSLegacyBundleMonkeyPatch;
import org.cocktail.fwkcktlwebapp.server.version.A_CktlVersion;

import com.webobjects.foundation.NSTimeZone;
import com.webobjects.foundation.NSTimestampFormatter;
import com.woinject.WOInject;

import er.extensions.appserver.ERXApplication;

public class Application extends CocktailAjaxApplication {
	private static final String CONFIG_FILE_NAME = VersionMe.APPLICATIONINTERNALNAME + ".config";
	private static final String CONFIG_TABLE_NAME = "FwkCktlWebApp_GrhumParametres";
	private static final String MAIN_MODEL_NAME = VersionMe.APPLICATIONINTERNALNAME;
	
	public static GspotParamManager gspotParamManager = new GspotParamManager();
	
	private final Logger logger = ERXApplication.log;
    
    /**
     * Liste des parametres obligatoires (dans fichier de config ou table grhum_parametres) pour que l'application se lance. 
     * Si un des parametre n'est pas initialisÃ©, il y a une erreur bloquante.
     */
    public static final String[] MANDATORY_PARAMS = new String[]{};
    
    /**
     * Liste des parametres optionnels (dans fichier de config ou table grhum_parametres). 
     * Si un des parametre n'est pas initialisÃ©, il y a un warning.
     */
    public static final String[] OPTIONAL_PARAMS = new String[]{};	
    
    /**
     * Mettre Ã  true pour que votre application renvoie les informations de collecte au serveur de collecte de Cocktail.
     */
    public static final boolean APP_SHOULD_SEND_COLLECTE = false;	
	
	public static void main(String[] argv) {
		//ERXDatabaseContextMulticastingDelegate.addDefaultDelegate(new MyDBDelegate());
				//new ERXEntityDependencyOrderingDelegate());  
		// Ensure that we get proper error pages when an exception happens in a direct action.  
		// This must go here and not in the constructor or it won't work.

        //System.setProperty("WODisplayExceptionPages", "true");

//		ERXApplication.main(argv, Application.class);
		NSLegacyBundleMonkeyPatch.apply();
    	WOInject.init("org.cocktail.gspot.serveur.Application", argv);
	}

	private Version _appVersion;
	
	public static NSTimeZone ntz = null;
	
	public Application() {
		super();
		/* Remplacement du requestHandler pour les ressources statiques */
		registerRequestHandler(new JarResourceRequestHandler(), "_wr_");
	}
	
	public void initApplication() {
		System.out.println("Lancement de l'application serveur " + this.name() + "...");
		super.initApplication();
		
		initTimeZones();
		rawLogAppInfos();
		rawLogVersionInfos();
		
		//Afficher les infos de connexion des modeles de donnees
		rawLogModelInfos();
		//Verifier la coherence des dictionnaires de connexion des modeles de donnees
		boolean isInitialisationErreur = !checkModel() ;
		
		//TODO ajouter votre code pour l'initialisation
		/*EODatabaseContext.setDefaultDelegate(new MyDBDelegate());
		ERXDatabaseContextMulticastingDelegate.addDefaultDelegate(new MyDBDelegate());
		*/
	}
	
	public String configFileName() {
		return CONFIG_FILE_NAME;
	}
	
	public String configTableName() {
		return CONFIG_TABLE_NAME;
	}
	
	public String[] configMandatoryKeys() {
		return MANDATORY_PARAMS;
	}
	
	public String[] configOptionalKeys() {
		return OPTIONAL_PARAMS;
	}
	
	public boolean appShouldSendCollecte() {
		return APP_SHOULD_SEND_COLLECTE;
	}

	public String copyright() {
		return appVersion().copyright();
	}
	
	public A_CktlVersion appCktlVersion() {
		return appVersion();
	}
	
    public Version appVersion() {
    	if (_appVersion == null) {
    		_appVersion = new Version();
    	}
    	return _appVersion;
    }
    
	public String mainModelName() {
		return MAIN_MODEL_NAME;
	}
	
	public boolean _isSupportedDevelopmentPlatform() {
		return (super._isSupportedDevelopmentPlatform() || System.getProperty("os.name").startsWith("Win"));
	}
	
	/**
     * Initialise le TimeZone a utiliser pour l'application.
     */
    protected void initTimeZones() {
    	System.out.println("Initialisation du NSTimeZone");
        String tz = config().stringForKey("DEFAULT_NS_TIMEZONE");
        if (tz == null || tz.equals("")) {
            CktlLog
            .log("Le parametre DEFAULT_NS_TIMEZONE n'est pas defini dans le fichier .config.");
            TimeZone.setDefault(TimeZone.getTimeZone("Europe/Paris"));
            NSTimeZone.setDefaultTimeZone(NSTimeZone.timeZoneWithName(
                    "Europe/Paris", false));
        } else {
            ntz = NSTimeZone.timeZoneWithName(tz, false);
            if (ntz == null) {
                CktlLog
                .log("Le parametre DEFAULT_NS_TIMEZONE defini dans le fichier .config n'est pas valide ("
                        + tz + ")");
                TimeZone.setDefault(TimeZone.getTimeZone("Europe/Paris"));
                NSTimeZone.setDefaultTimeZone(NSTimeZone.timeZoneWithName(
                        "Europe/Paris", false));
            } else {
                TimeZone.setDefault(ntz);
                NSTimeZone.setDefaultTimeZone(ntz);
            }
        }
        ntz = NSTimeZone.defaultTimeZone();
        System.out.println("NSTimeZone par defaut utilise dans l'application:"
                + NSTimeZone.defaultTimeZone());
        NSTimestampFormatter ntf = new NSTimestampFormatter();
        System.out.println("Les NSTimestampFormatter analyseront les dates avec le NSTimeZone: "
                + ntf.defaultParseTimeZone());
        System.out.println("Les NSTimestampFormatter afficheront les dates avec le NSTimeZone: "
                + ntf.defaultFormatTimeZone());
    }
    
    @Override
	public void finishInitialization() {
		// TODO Auto-generated method stub
		super.finishInitialization();
		
		gspotParamManager.checkAndInitParamsWithDefault();
	}
	
	public Logger logger() {
        return logger;
    }
}
