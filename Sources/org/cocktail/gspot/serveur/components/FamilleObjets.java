package org.cocktail.gspot.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.gspot.serveur.Session;

import com.webobjects.appserver.WOContext;

public class FamilleObjets extends CktlAjaxWOComponent {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public FamilleObjets(WOContext context) {
        super(context);
    }
	
	public boolean canEditFamille() {
		
		return ((Session)session()).gspotUser().canEditRefTable();
	}
}