package org.cocktail.gspot.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkgspot.serveur.metier.eof.EOSalles;
import org.cocktail.fwkgspotguiajax.serveur.components.GspotBaseComponent;
import org.cocktail.gspot.serveur.Session;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WORedirect;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.appserver.WOActionResults;

import er.ajax.AjaxModalDialog;
import er.extensions.appserver.ERXRedirect;

public class TabInfosSalles extends GspotBaseComponent {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TabInfosSalles(WOContext context) {
        super(context);
    }
    
    private boolean isEditedLocalisation;

	/**
	 * @return the isEditedLocalisation
	 */
	public boolean isEditedLocalisation() {
		return isEditedLocalisation;
	}

	/**
	 * @param isEditedLocalisation the isEditedLocalisation to set
	 */
	public void setIsEditedLocalisation(boolean isEditedLocalisation) {
		this.isEditedLocalisation = isEditedLocalisation;
	}
	
	public String aucbtblocalisationid() {
		return getComponentId()+"_aucbtblocalisationid";
	}
	
	private boolean editedInfos;

	/**
	 * @return the editedInfos
	 */
	public boolean editedInfos() {
		return editedInfos;
	}

	/**
	 * @param editedInfos the editedInfos to set
	 */
	public void setEditedInfos(boolean editedInfos) {
		this.editedInfos = editedInfos;
	}
	
	public String aucbtbinfosalleid() {
		return getComponentId()+"_aucbtbinfosalleid";
	}
	public String auclocalisationid() {
		return getComponentId()+"_auclocalisationid";
	}

	public String aucinfossalleid() {
		return getComponentId()+"_aucinfossalleid";
	}

	public String aucaccessalleid() {
		return getComponentId()+"_aucaccessalleid";
	}

	private boolean editedAcces;
	private NSArray<String> lstInfosIds;

	/**
	 * @return the editedAcces
	 */
	public boolean editedAcces() {
		return editedAcces;
	}

	/**
	 * @param editedAcces the editedAcces to set
	 */
	public void setEditedAcces(boolean editedAcces) {
		this.editedAcces = editedAcces;
	}

	public String aucbtbaccesid() {
		return getComponentId()+"_aucbtbaccesid";
	}

	public String auctriginfosid() {
		return getComponentId()+"_auctriginfosid";
	}

	/**
	 * @return the lstInfosIds
	 */
	public NSArray<String> lstInfosIds() {
		if (lstInfosIds==null){
			lstInfosIds = new NSArray<String>(new String[]{aucbtbinfosalleid(),aucaccessalleid()});
		}
		return lstInfosIds;
	}

	public boolean canDeleteSalle() {
		return (deletedSalle()!=null)&&getGspotUser().canEditSalle(deletedSalle());
	}

	public WOActionResults deleteSalle() {
		deletedSalle().setDAnnulation(new NSTimestamp());
		deletedSalle().editingContext().saveChanges();
		if (deletedSalle().editingContext().parentObjectStore().equals(
				session().defaultEditingContext()))
			session().defaultEditingContext().saveChanges();
		
		isDeleted = true;
		AjaxModalDialog.update(context(), null);
		
		return null;
	}

	public String cawdeletesalleid() {
		return getComponentId()+"_cawdeletesalleid";
	}

	public boolean isDeleted = false;
	
	public WOActionResults openDelDialog() {
		isDeleted = false;
		AjaxModalDialog.open(context(), cawdeletesalleid(), "Suppression de la salle");
		return null;
	}

	public WOActionResults cancelDelete() {
		AjaxModalDialog.close(context());
		return null;
	}
	
	public EOSalles deletedSalle(){
		return ((Session)session()).selectedSalle();
	}

	public String aucdelwindowid() {
		return getComponentId()+"_aucdelwindowid";
	}

	public WOActionResults retourDelete() {
		((Session)session()).setSelectedSalle(null);
		String url = context().directActionURLForActionNamed(
				"default", null);
		ERXRedirect redirect = new ERXRedirect(context());
		redirect.setUrl(url);
		return redirect;
	}

	
}