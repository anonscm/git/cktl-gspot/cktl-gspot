package org.cocktail.gspot.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

public class TabEquipement extends CktlAjaxWOComponent {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TabEquipement(WOContext context) {
        super(context);
    }
    private boolean isEditedEquipement;

	/**
	 * @return the isEditedEquipement
	 */
	public boolean isEditedEquipement() {
		return isEditedEquipement;
	}

	/**
	 * @param isEditedEquipement the isEditedEquipement to set
	 */
	public void setEditedEquipement(boolean isEditedEquipement) {
		this.isEditedEquipement = isEditedEquipement;
	}
	public String lstequipementid() {
		return getComponentId() + "_lstequipementid";
	}

	public String aucbtbequipementid() {
		return getComponentId() + "_aucbtbequipementid";
	}

	public String auctrigelementid() {
		return getComponentId() + "_auctrigelementid";
	}
	private NSArray<String> lstEquipementZones;
	private boolean editedPrises;

	public NSArray<String> lstEquipementZones() {
		if (lstEquipementZones == null) {
			lstEquipementZones = new NSArray<String>(new String[] {
					aucbtbequipementid(), lstequipementid()});
		}
		return lstEquipementZones;
	}
	private boolean editedTel;
	private NSArray<String> lstPriseZones;
	public NSArray<String> lstPriseZones() {
		if (lstPriseZones == null) {
			lstPriseZones = new NSArray<String>(new String[] {
					aucbtbprisetid(), aucLstPrisesid()});
		}
		return lstPriseZones;
	}
	public String aucprisesid() {
		return getComponentId()+"_aucprisesid";
	}

	/**
	 * @return the editedPrises
	 */
	public boolean editedPrises() {
		return editedPrises;
	}

	/**
	 * @param editedPrises the editedPrises to set
	 */
	public void setEditedPrises(boolean editedPrises) {
		this.editedPrises = editedPrises;
	}

	public String aucbtbprisetid() {
		return getComponentId()+"_aucbtbprisetid";
	}

	public String aucLstPrisesid() {
		return getComponentId() + "_aucLstPrisesid";
	}

	public String auctrigpriseid() {
		return getComponentId() + "_auctrigpriseid";
	}

	/**
	 * @return the editedTel
	 */
	public boolean editedTel() {
		return editedTel;
	}

	/**
	 * @param editedTel the editedTel to set
	 */
	public void setEditedTel(boolean editedTel) {
		this.editedTel = editedTel;
	}

	public String auctrigtelid() {
		return getComponentId() + "_auctrigtelid";
	}
	public String aucbtbteltid() {
		return getComponentId()+"_aucbtbtelid";
	}

	public String aucLsttelid() {
		return getComponentId() + "_aucLsttelid";
	}

	public String auctelsid() {
		return getComponentId() + "_auctelsid";
	}
	private NSArray<String> lstTelZones;
	public NSArray<String> lstTelZones() {
		if (lstTelZones == null) {
			lstTelZones = new NSArray<String>(new String[] {
					aucbtbteltid(), aucLsttelid()});
		}
		return lstTelZones;
	}
	
	private NSArray<String> lstPortesZones;
	public NSArray<String> lstPortesZones() {
		if (lstPortesZones == null) {
			lstPortesZones = new NSArray<String>(new String[] {
					aucbtbportesid(), aucLstPortesid()});
		}
		return lstPortesZones;
	}
	public String aucportesid() {
		return getComponentId()+"_aucportesid";
	}
	
	private boolean editedPortes;

	public boolean editedPortes() {
		return editedPortes;
	}
	
	public void setEditedPortes(boolean editedPortes) {
		this.editedPortes = editedPortes;
	}

	public String aucbtbportesid() {
		return getComponentId()+"_aucbtbportesid";
	}

	public String aucLstPortesid() {
		return getComponentId() + "_aucLstPortesid";
	}

	public String auctrigportesid() {
		return getComponentId() + "_auctrigportesid";
	}
}