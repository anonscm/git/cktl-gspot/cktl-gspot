package org.cocktail.gspot.serveur.components;

import java.util.GregorianCalendar;
import java.util.Iterator;

import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonne;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.server.components.CktlWebPage;
import org.cocktail.fwkcktlwebapp.server.util.EOModelCtrl;
import org.cocktail.gspot.serveur.GspotParamManager;
import org.cocktail.gspot.serveur.VersionMe;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WORedirect;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eoaccess.EOModel;
import com.webobjects.eoaccess.EOModelGroup;
import com.webobjects.foundation.NSMutableArray;

public class Wrapper extends CktlWebPage {
	/**
	 * 
	 */
	private static final long serialVersionUID = -16040081176556634L;
	private String connectionBase;

	public String getTitre() {
		return (String) valueForBinding("titre");
	}

	public void setTitre(String titre) {
		setValueForBinding(titre,"titre");
	}

	public Wrapper(WOContext context) {
		super(context);
	}

	public String copyright() {
		return "(c) " + DateCtrl.nowDay().get(GregorianCalendar.YEAR);
	}

	public String version() {
		return VersionMe.appliVersion();
	}

	public boolean isConnected() {
		return (cktlSession().connectedUserInfo() != null);
	}

	/**
	 * @return the connectionBase
	 */
	public String connectionBase() {
		if (connectionBase == null) {
			connectionBase = "";
			EOModelGroup vModelGroup = EOModelGroup.defaultGroup();
			NSMutableArray<String> lstUrl = new NSMutableArray<String>();
			for (int i = 0; i < vModelGroup.models().count(); i++) {
				EOModel tmpEOModel = (EOModel) vModelGroup.models()
						.objectAtIndex(i);
				String urlModel = EOModelCtrl.bdConnexionUrl(tmpEOModel);
				urlModel = urlModel.substring(urlModel.lastIndexOf(":") + 1);
				if (!lstUrl.contains(urlModel))
					lstUrl.addObject(urlModel);
			}
			Iterator<String> it = lstUrl.iterator();
			while (it.hasNext()) {
				connectionBase += (String) it.next() + "<br>";
			}
		}

		return connectionBase;
	}

	public WOActionResults killSession() {
		session().terminate();
		String url = context().directActionURLForActionNamed("default", null);
		WORedirect redirect = new WORedirect(context());
		redirect.setUrl(url);
		return redirect;
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
		//ERXResponseRewriter.addScriptResourceInHead(response, context, "app","scripts/gspot.js");
	}

	public String onLoadWarpper() {
		String retour = "initMenu();";
		if ((hasBinding("onLoadPage")) && (!"".equals(valueForBinding("onLoadPage"))))
			 retour+=valueForBinding("onLoadPage");		
		return retour;
	}

	@Override
	public boolean synchronizesVariablesWithBindings() {		
		return false;
	}
	
	/**
	 * Ajout d'un paramètre dans la table Grhum_Paramètres pour choisir le CSS adapté à son établissement.
	 */
	public String cssEtablissement(){
		if (GspotParamManager.GSPOT_CSS != null
				&& !GspotParamManager.GSPOT_CSS.equals("")
				&& !GspotParamManager.GSPOT_CSS.equals(" ")){
			String chemin = FwkCktlPersonne.paramManager.getParam(GspotParamManager.GSPOT_CSS);
			return "css/" + chemin;
		}
		return "css/CktlCommonOrange.css";
	}
}