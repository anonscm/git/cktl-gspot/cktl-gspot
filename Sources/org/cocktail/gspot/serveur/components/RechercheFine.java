package org.cocktail.gspot.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkgspot.serveur.metier.eof.EOSalles;
import org.cocktail.gspot.serveur.Session;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.foundation.NSDictionary;

public class RechercheFine extends CktlAjaxWOComponent {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private EOSalles selectedSalle;

	public RechercheFine(WOContext context) {
		super(context);
	}

	public String aucselectedsalleid() {
		return getComponentId() + "_aucselectedsalleid";
	}

	/**
	 * @return the selectedSalle
	 */
	public EOSalles selectedSalle() {
		return selectedSalle;
	}

	/**
	 * @param selectedSalle
	 *            the selectedSalle to set
	 */
	public void setSelectedSalle(EOSalles selectedSalle) {
		this.selectedSalle = selectedSalle;
	}

	public WOActionResults goToGestionSalle() {
		((Session) session()).setSelectedSalle(selectedSalle());

		SalleGestion salleGest = (SalleGestion) pageWithName(SalleGestion.class.getName());
		salleGest.setEdited(false);
		((Session) session()).setSelectedOnglet(TabInfosSalles.class.getName());

		return salleGest;
	}

	public String goGestionSalleLib() {
		NSDictionary<String, Object> pkDico = EOUtilities.primaryKeyForObject(edc(), selectedSalle());
		Long salNumero = (Long) pkDico.objectForKey("salNumero");

		return "Aller à la gestion de la salle : " + selectedSalle().salPorte() + " (" + salNumero + ")";
	}
}