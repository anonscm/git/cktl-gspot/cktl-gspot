package org.cocktail.gspot.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.gspot.serveur.Session;

import com.webobjects.appserver.WOContext;

public class TypeAccesHandicape extends CktlAjaxWOComponent {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public TypeAccesHandicape(WOContext context) {
        super(context);
    }
    public boolean canEditType() {
		return ((Session)session()).gspotUser().canEditRefTable();
	}
}