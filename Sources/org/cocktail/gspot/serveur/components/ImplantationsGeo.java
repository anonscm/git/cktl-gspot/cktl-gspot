package org.cocktail.gspot.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.gspot.serveur.Session;

import com.webobjects.appserver.WOContext;

public class ImplantationsGeo extends CktlAjaxWOComponent {
    public ImplantationsGeo(WOContext context) {
        super(context);
    }

	public boolean canEditImplantation() {
	
		return ((Session)session()).gspotUser().canEditRefTable();
	}

	
}