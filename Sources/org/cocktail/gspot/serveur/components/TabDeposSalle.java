package org.cocktail.gspot.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

public class TabDeposSalle extends CktlAjaxWOComponent {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TabDeposSalle(WOContext context) {
		super(context);
	}

	private boolean editedGrpDepos;

	/**
	 * @return the editedGrpDepos
	 */
	public boolean editedGrpDepos() {
		return editedGrpDepos;
	}

	/**
	 * @param editedGrpDepos
	 *            the editedGrpDepos to set
	 */
	public void setEditedGrpDepos(boolean editedGrpDepos) {
		this.editedGrpDepos = editedGrpDepos;
	}

	public String aucbtboccupationid() {
		return getComponentId() + "_aucbtboccupationid";
	}

	public String aucbtbdeposid() {
		return getComponentId() + "_aucbtbdeposid";
	}

	public String aucinddeposid() {
		return getComponentId() + "_aucinddeposid";
	}

	public String aucbtbdeposindid() {
		return getComponentId() + "_aucbtbdeposindid";
	}

	private boolean editedIndivDepos;

	/**
	 * @return the editedIndivDepos
	 */
	public boolean editedIndivDepos() {
		return editedIndivDepos;
	}

	/**
	 * @param editedIndivDepos
	 *            the editedIndivDepos to set
	 */
	public void setEditedIndivDepos(boolean editedIndivDepos) {
		this.editedIndivDepos = editedIndivDepos;
	}

	public String aucgrpdeposid() {
		return getComponentId() + "_aucgrpdeposid";
	}

	public String aucoccupationsid() {
		return getComponentId() + "_aucoccupationsid";
	}

	private boolean editedOccupation;

	/**
	 * @return the editedOccupation
	 */
	public boolean editedOccupation() {
		return editedOccupation;
	}

	/**
	 * @param editedOccupation
	 *            the editedOccupation to set
	 */
	public void setEditedOccupation(boolean editedOccupation) {
		this.editedOccupation = editedOccupation;
	}

	public String aucTriggerEditOccupid() {
		return getComponentId() + "_aucTriggerEditOccupid";
	}

	private NSArray<String> lstEditOccupZones = new NSArray<String>(
			new String[] { aucoccupationsid(), zonebuttonid() });
	private boolean editedOccupants;
	private Boolean refreshDepos;
	private NSArray<String> lstIdsTrigOccup;;

	/**
	 * @return the lstSearchZones
	 */
	public NSArray<String> lstEditOccupZones() {
		return lstEditOccupZones;
	}

	public String zonebuttonid() {
		return getComponentId() + "_zonebuttonoccupid";
	}

	/**
	 * @return the editedOccupants
	 */
	public boolean editedOccupants() {
		return editedOccupants;
	}

	/**
	 * @param editedOccupants
	 *            the editedOccupants to set
	 */
	public void setEditedOccupants(boolean editedOccupants) {
		this.editedOccupants = editedOccupants;
	}

	public String aucbtboccupsid() {
		return getComponentId() + "_aucbtboccupsid";
	}

	public String aucgrpoccupsid() {
		return getComponentId() + "_aucgrpoccupsid";
	}

	/**
	 * @return the refreshDepos
	 */
	public Boolean refreshDepos() {
		return refreshDepos;
	}

	/**
	 * @param refreshDepos
	 *            the refreshDepos to set
	 */
	public void setRefreshDepos(Boolean refreshDepos) {
		this.refreshDepos = refreshDepos;
	}

	public String auctrigoccupid() {
		return getComponentId() + "_auctrigoccupid";
	}

	/**
	 * @return the lstIdsTrigOccup
	 */
	public NSArray<String> lstIdsTrigOccup() {
		if (lstIdsTrigOccup == null) {
			lstIdsTrigOccup = new NSArray<String>(new String[] {
					aucbtboccupationid(), aucinddeposid() });
		}
		return lstIdsTrigOccup;
	}

	/**
	 * @param lstIdsTrigOccup
	 *            the lstIdsTrigOccup to set
	 */
	public void setLstIdsTrigOccup(NSArray<String> lstIdsTrigOccup) {
		this.lstIdsTrigOccup = lstIdsTrigOccup;
	}

}