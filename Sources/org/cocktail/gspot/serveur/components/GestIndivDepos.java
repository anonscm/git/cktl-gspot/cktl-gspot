package org.cocktail.gspot.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkgspot.serveur.finder.FinderDepositaires;
import org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr;
import org.cocktail.gspot.serveur.Session;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class GestIndivDepos extends CktlAjaxWOComponent {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public GestIndivDepos(WOContext context) {
		super(context);
	}

	public String divIdeposId() {
		return getComponentId() + "_divIdeposId";
	}

	public String contentid() {
		return getComponentId() + "_contentid";
	}

	private EOStructureUlr selectedService;

	public EOStructureUlr getSelectedService() {
		return selectedService;
	}

	public void setSelectedService(EOStructureUlr selectedService) {
		this.selectedService = selectedService;
		deposForService();
	}

	public String ideposid() {
		return getComponentId() + "_ideposid";
	}

	private WODisplayGroup dgDepos;

	/**
	 * @return the dgDepos
	 */
	public WODisplayGroup dgDepos() {
		if (dgDepos == null) {
			dgDepos = new WODisplayGroup();
			deposForService();
		}
		return dgDepos;
	}

	public WOActionResults deposForService() {
		if (selectedService != null) {
			if (dgDepos == null)
				dgDepos = new WODisplayGroup();
			dgDepos.setObjectArray(EORepartAssociation
					.fetchAll(
							session().defaultEditingContext(),
							new EOAndQualifier(
									new NSArray<EOQualifier>(
											new EOQualifier[] {
													FinderDepositaires.QUAL_REPART_ASSOC_DEPOS,
													EOQualifier
															.qualifierWithQualifierFormat(
																	EORepartAssociation.TO_STRUCTURE_KEY
																			+ "=%@",
																	new NSArray<EOStructureUlr>(
																			getSelectedService())) }))));

		} else {
			dgDepos.setObjectArray(null);
		}
		dgDepos.clearSelection();
		return null;

	}

	/**
	 * @param dgDepos
	 *            the dgDepos to set
	 */
	public void setDgDepos(WODisplayGroup dgDepos) {
		this.dgDepos = dgDepos;
	}

	private boolean editedIndivDepos;

	/**
	 * @return the editedIndivDepos
	 */
	public boolean editedIndivDepos() {
		return editedIndivDepos;
	}

	/**
	 * @param editedIndivDepos
	 *            the editedIndivDepos to set
	 */
	public void setEditedIndivDepos(boolean editedIndivDepos) {
		this.editedIndivDepos = editedIndivDepos;
	}

	public String aucbtbdeposindid() {
		return getComponentId() + "_aucbtbdeposindid";
	}

	public String aucinddeposid() {
		return getComponentId() + "_aucinddeposid";
	}

	public boolean canEditDepos() {
		if (getSelectedService() != null)
			return ((Session) session()).gspotUser()
					.canEditRoleDepositaireStructure(
							getSelectedService().toFwkpers_Structure());
		return false;
	}

	public String buzzyid() {
		return getComponentId() + "_buzzyid";
	}

	public String listeVideMesage() {

		if (selectedService != null) {
			return "Aucun dépositaire pour ce service";
		} else {
			return "Choisissez un service";
		}

	}

}