package org.cocktail.gspot.serveur.components;

import org.cocktail.fwkcktlpersonneguiajax.serveur.components.AComponent;
import org.cocktail.fwkgspot.serveur.metier.eof.EOSalles;
import org.cocktail.gspot.serveur.Session;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

public class Menu extends AComponent {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Menu(WOContext context) {
		super(context);
	}

	public WOActionResults goToEditSalle() {
		SalleGestion next = (SalleGestion) pageWithName(SalleGestion.class
				.getName());
		next.setEdited(false);
		((Session) session()).setSelectedOnglet(TabInfosSalles.class.getName());
		return next;
	}

	public WOActionResults goToCreatSalle() {

		CreateSalle next = (CreateSalle) pageWithName(CreateSalle.class
				.getName());
		next.setSelectedSalle(EOSalles.createFwkGspot_Salles(
				new EOEditingContext(session().defaultEditingContext()),
				new NSTimestamp(), new NSTimestamp()));
		return next;
	}

	public WOActionResults goToDeposIndiv() {
		return pageWithName(GestIndivDepos.class.getName());
	}

	public WOActionResults goToLotSalle() {
		return pageWithName(LotsSalles.class.getName());
	}

}