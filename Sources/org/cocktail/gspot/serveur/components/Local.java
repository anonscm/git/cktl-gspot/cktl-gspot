package org.cocktail.gspot.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.gspot.serveur.Session;

import com.webobjects.appserver.WOContext;

public class Local extends CktlAjaxWOComponent {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Local(WOContext context) {
        super(context);
    }

	public boolean canEditLocal() {
		return ((Session)session()).gspotUser().canEditRefTable();
	}

	
}