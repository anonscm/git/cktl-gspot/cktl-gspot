package org.cocktail.gspot.serveur.components;

import java.util.UUID;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkgspotguiajax.serveur.components.SallesTreeDelegate;
import org.cocktail.gspot.serveur.Session;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

import er.ajax.AjaxUtils;
import er.extensions.foundation.ERXStringUtilities;

public class SalleGestion extends CktlAjaxWOComponent {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private SallesTreeDelegate treeDelegate;

	public SalleGestion(WOContext context) {
		super(context);
		setLstRefreshZones(new NSArray<String>(new String[] { divSalleId(),
				"ErreurContainer" }));
		setLstRefreshZonesSearch(new NSArray<String>(new String[] {
				divSearchId(), "ErreurContainer", aucbtbsearchid() }));
	}

	private String divSearchId = ERXStringUtilities
			.safeIdentifierName("searchSalle" + UUID.randomUUID());

	/**
	 * @return the divSearchId
	 */
	public String divSearchId() {
		return divSearchId;
	}

	/**
	 * @param divSearchId
	 *            the divSearchId to set
	 */
	public void setDivSearchId(String divSearchId) {
		this.divSearchId = divSearchId;
	}

	public SallesTreeDelegate getTreeDelegate() {

		if ((treeDelegate != null)
				&& (treeDelegate instanceof SallesTreeDelegate)) {

		} else {
			treeDelegate = new SallesTreeDelegate(session()
					.defaultEditingContext());

		}

		return treeDelegate;
	}

	public void setTreeDelegate(SallesTreeDelegate treeDelegate) {
		this.treeDelegate = treeDelegate;
	}

	private String divSalleId = ERXStringUtilities.safeIdentifierName("salle"
			+ UUID.randomUUID());

	/**
	 * @return the divSalleId
	 */
	public String divSalleId() {
		return divSalleId;
	}

	/**
	 * @param divSalleId
	 *            the divSalleId to set
	 */
	public void setDivSalleId(String divSalleId) {
		this.divSalleId = divSalleId;
	}

	private Boolean searchVisible = Boolean.FALSE;

	/**
	 * @return the searchVisible
	 */
	public boolean searchVisible() {
		return searchVisible;
	}

	/**
	 * @param searchVisible
	 *            the searchVisible to set
	 */
	public void setSearchVisible(boolean searchVisible) {
		this.searchVisible = searchVisible;

	}

	public WOActionResults showSearch() {
		if (!searchVisible()) {
			setSearchVisible(true);
			// minimizedSearch=Boolean.FALSE;
			//openWinSearch();
		}
		return null;
	}

	public WOActionResults openWinSearch() {
		if (searchVisible()) {
			CktlAjaxWindow.open(context(), cadSearchid(),
					"Recherche de salle");
			/*
			 * if (minimizedSearch) AjaxUtils.javascriptResponse(
			 * " var win = Windows.getWindow('cadSearchid_win');win.minimize();"
			 * , //"var win = Windows.getWindow('"+ cadSearchid() +
			 * "_win');win.doMinimize=true;", context());
			 */
		}
		return null;
	}

	public WOActionResults winSearchMinimize() {
		if (minimizedSearch)
			AjaxUtils
					.javascriptResponse(
							//" var win = Windows.getWindow('cadSearchid_win');win.minimize();",
							 "var win = Windows.getWindow('"+ cadSearchid() +
							 "_win');win.doMinimize=true;",
							context());
		return null;
	}

	private boolean edited;

	/**
	 * @return the edited
	 */
	public boolean isEdited() {
		return edited;
	}

	/**
	 * @param edited
	 *            the edited to set
	 */
	public void setEdited(boolean edited) {
		this.edited = edited;
	}

	private NSArray<String> lstRefreshZones;

	/**
	 * @return the lstRefreshZones
	 */
	public NSArray<String> lstRefreshZones() {
		return lstRefreshZones;
	}

	/**
	 * @param lstRefreshZones
	 *            the lstRefreshZones to set
	 */
	public void setLstRefreshZones(NSArray<String> lstRefreshZones) {
		this.lstRefreshZones = lstRefreshZones;
	}

	public boolean commitOnValid() {

		return true;
	}

	public String classInfosSalle() {
		if (TabInfosSalles.class.getName().equals(
				((Session) session()).getSelectedOnglet()))
			return "selectedTab";
		return "";
	}

	public WOActionResults goInfosSalle() {
		closeSearch();
		((Session) session()).setSelectedOnglet(TabInfosSalles.class.getName());
		return null;
	}

	public String classDeposSalle() {
		if (TabDeposSalle.class.getName().equals(
				((Session) session()).getSelectedOnglet()))
			return "selectedTab";
		return "";
	}

	public WOActionResults goDeposSalle() {
		closeSearch();
		((Session) session()).setSelectedOnglet(TabDeposSalle.class.getName());
		return null;
	}

	public String cadSearchid() {
		return getComponentId() + "_cadSearchid";
	}

	public String aucbtbsearchid() {
		return getComponentId() + "_aucbtbsearchid";
	}

	public String auctriggersearchid() {
		return getComponentId() + "_auctriggersearchid";
	}

	public String aucTriggerSalleId() {
		return getComponentId() + "_aucTriggerSalleId";
	}

	private NSArray<String> lstRefreshZonesSearch;

	/**
	 * @return the lstRefreshZonesSearch
	 */
	public NSArray<String> lstRefreshZonesSearch() {
		return lstRefreshZonesSearch;
	}

	/**
	 * @param lstRefreshZonesSearch
	 *            the lstRefreshZonesSearch to set
	 */
	public void setLstRefreshZonesSearch(NSArray<String> lstRefreshZonesSearch) {
		this.lstRefreshZonesSearch = lstRefreshZonesSearch;
	}

	public WOActionResults closeSearch() {
		setSearchVisible(false);
		return null;
	}

	private Boolean minimizedSearch = Boolean.FALSE;

	private Boolean closeOnSelect=Boolean.TRUE;

	public WOActionResults minimizeSearch() {
		minimizedSearch = !minimizedSearch;
		// System.out.println(minimizedSearch);
		return null;
	}

	public WOActionResults showSearchOnNeed() {
		if (((Session) session()).selectedSalle() == null)
			showSearch();
		return null;
	}

	public WOActionResults goEquipement() {
		closeSearch();
		((Session) session()).setSelectedOnglet(TabEquipement.class.getName());
		return null;
	}

	public String classEquipement() {
		if (TabEquipement.class.getName().equals(
				((Session) session()).getSelectedOnglet()))
			return "selectedTab";
		return "";
	}

	public String buzzyid() {
		return getComponentId()+"_buzzid";
	}

	/**
	 * @return the closeOnSelect
	 */
	public Boolean closeOnSelect() {
		return closeOnSelect;
	}

	/**
	 * @param closeOnSelect the closeOnSelect to set
	 */
	public void setCloseOnSelect(Boolean closeOnSelect) {
		this.closeOnSelect = closeOnSelect;
	}

	public WOActionResults onSelectSalle() {
		if (closeOnSelect()){
			CktlAjaxWindow.close(context(), cadSearchid());
		}
		return null;
	}

}