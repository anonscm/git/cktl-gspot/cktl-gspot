package org.cocktail.gspot.serveur.components;

import org.cocktail.fwkgspot.serveur.metier.eof.EOSalles;
import org.cocktail.fwkgspotguiajax.serveur.UtilMessages;
import org.cocktail.fwkgspotguiajax.serveur.components.GspotBaseComponent;
import org.cocktail.gspot.serveur.Session;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.foundation.NSArray;

public class CreateSalle extends GspotBaseComponent {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private EOSalles selectedSalle;

	public CreateSalle(WOContext context) {
		super(context);
	}

	public String divSalleId() {
		return getComponentId() + "_divsalleid";
	}

	public String aucTriggerSalleId() {
		return getComponentId() + "_aucTriggerSalleId";
	}

	private NSArray<String> lstRefreshZones;

	/**
	 * @return the lstRefreshZones
	 */
	public NSArray<String> lstRefreshZones() {
		if (lstRefreshZones==null)
			lstRefreshZones=new NSArray<String>(new String[] { 
			messageLocalisationId(),"jscontainerid" });
		return lstRefreshZones;
	}


	public String contentid() {
		return getComponentId() + "_contentid";
	}

	public EOSalles getSelectedSalle() {
		return selectedSalle;
	}

	public void setSelectedSalle(EOSalles selectedSalle) {
		this.selectedSalle = selectedSalle;
	}

	public WOActionResults validCreat() {
		onSuccesValid="";
		if (getSelectedSalle().toLocal() == null) {
			UtilMessages.creatMessageUtil(session(), UtilMessages.ERROR_MESSAGE,
					"Le bâtiment est obligatoire !!");
			return null;
		}

		if ((getSelectedSalle().salEtage() == null)
				|| ("".equals(getSelectedSalle().salEtage()))) {
			UtilMessages.creatMessageUtil(session(), UtilMessages.ERROR_MESSAGE,
					"L'étage est obligatoire !!");

			return null;
		}
		if ((getSelectedSalle().salPorte() == null)
				|| ("".equals(getSelectedSalle().salPorte()))) {
			UtilMessages.creatMessageUtil(session(), UtilMessages.ERROR_MESSAGE,
					"Le nom est obligatoire !!");
			return null;
		}

		getSelectedSalle().editingContext().saveChanges();
		if (getSelectedSalle().editingContext().parentObjectStore().equals(
				session().defaultEditingContext())) {
			session().defaultEditingContext().saveChanges();
		}
		((Session) session()).setSelectedSalle((EOSalles) EOUtilities
				.localInstanceOfObject(session().defaultEditingContext(),
						getSelectedSalle()));

		((Session) session()).setSelectedSalle(getSelectedSalle());
		String url = context().directActionURLForActionNamed(
				"goToEdit", null);
		onSuccesValid="document.location='"+url+"';";
		return null;

	}

	

	/**
	 * @return the messageLocalisationId
	 */
	public String messageLocalisationId() {
		return getComponentId()+"_messageLocalisationId";
	}

	

	private String onSuccesValid="";

	/**
	 * @return the onSuccesValid
	 */
	public String onSuccesValid() {
		return onSuccesValid;
	}

	/**
	 * @param onSuccesValid the onSuccesValid to set
	 */
	public void setOnSuccesValid(String onSuccesValid) {
		this.onSuccesValid = onSuccesValid;
	}
	

	
}