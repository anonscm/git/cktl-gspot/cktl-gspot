package org.cocktail.gspot.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkgspot.serveur.finder.FinderDepositaires;
import org.cocktail.fwkgspot.serveur.metier.eof.EOLotSalle;
import org.cocktail.fwkgspot.serveur.metier.eof.EORepartBureau;
import org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotIndividu;
import org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotSalle;
import org.cocktail.fwkgspot.serveur.metier.eof.EOSalles;
import org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr;
import org.cocktail.fwkgspot.serveur.metier.eof.EOTypeOccupation;
import org.cocktail.fwkgspot.serveur.metier.eof.EOTypeSalle;
import org.cocktail.fwkgspotguiajax.serveur.UtilMessages;
import org.cocktail.fwkgspotguiajax.serveur.components.SallesTreeDelegate;
import org.cocktail.fwkgspotguiajax.serveur.components.BaseGestionRef.DgDelegate;
import org.cocktail.gspot.serveur.Session;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

import er.ajax.AjaxModalDialog;
import er.ajax.AjaxUpdateContainer;
import er.extensions.appserver.ERXRedirect;
import er.extensions.eof.ERXQ;

public class LotsSalles extends CktlAjaxWOComponent {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LotsSalles(WOContext context) {
		super(context);
	}

	public String auclisteid() {
		return getComponentId() + "_auclisteid";
	}

	private String searchLib;

	/**
	 * @return the searchLib
	 */
	public String searchLib() {
		return searchLib;
	}

	/**
	 * @param searchLib
	 *            the searchLib to set
	 */
	public void setSearchLib(String searchLib) {
		this.searchLib = searchLib;
	}

	private WODisplayGroup displayGroup;

	public WOActionResults searchObj() {
		displayGroup.setObjectArray(getLstLots());
		displayGroup.clearSelection();
		displayGroup.updateDisplayedObjects();
		displayGroup.setCurrentBatchIndex(1);
		return null;
	}

	public void setDisplayGroup(WODisplayGroup displayGroup) {
		this.displayGroup = displayGroup;
	}

	private NSArray<EOLotSalle> getLstLots() {
		String qualStr = "";
		NSMutableArray<Object> qualArray = new NSMutableArray<Object>();

		if ((searchLib() != null) && (!"".equals(searchLib().trim()))) {
			qualStr = EOLotSalle.LOT_LIBELLE_KEY + " caseInsensitiveLike %@ ";
			qualArray.add("*" + searchLib() + "*");
		}
		if (salleSearch() != null) {
			if (!"".equals(qualStr)) {
				qualStr += " AND ";
			}
			qualStr += ERXQ.keyPath(EOLotSalle.TO_REPART_LOT_SALLES_KEY,
					EORepartLotSalle.TO_SALLES_KEY) + " = %@ ";
			qualArray.add(salleSearch());
		}

		if (indivSearch() != null) {
			if (!"".equals(qualStr)) {
				qualStr += " AND ";
			}
			qualStr += ERXQ.keyPath(EOLotSalle.TO_REPART_LOT_INDIVIDUS_KEY,
					EORepartLotIndividu.TO_FWKPERS__INDIVIDU_KEY) + " = %@ ";
			qualArray.add(indivSearch());
		}

		if (ownerSearch() != null) {
			if (!"".equals(qualStr)) {
				qualStr += " AND ";
			}
			qualStr += ERXQ.keyPath(EOLotSalle.TO_FWKPERS__INDIVIDU_KEY)
					+ " = %@ ";
			qualArray.add(ownerSearch());
		}

		return EOLotSalle.fetchFwkGspot_LotSalles(session()
				.defaultEditingContext(), EOQualifier
				.qualifierWithQualifierFormat(qualStr, qualArray),
				new NSArray<EOSortOrdering>(new EOSortOrdering[] {
				/*
				 * new EOSortOrdering( EOLotSalle.TO_FWKPERS__INDIVIDU_KEY,
				 * EOSortOrdering.CompareAscending), //
				 */
				new EOSortOrdering(EOLotSalle.LOT_LIBELLE_KEY,
						EOSortOrdering.CompareCaseInsensitiveAscending) }));

	}

	public WODisplayGroup displayGroup() {

		if (displayGroup == null) {
			displayGroup = new WODisplayGroup();
			displayGroup.setDelegate(new DgDelegate());
			displayGroup.setNumberOfObjectsPerBatch(8);
			searchObj();
		}

		return displayGroup;
	}

	private EOLotSalle selectedLot;
	private NSArray<String> lstDelZones;
	private NSArray<String> lstDelSalleZones;
	private NSArray<String> lstDelIndZones;

	public void setSelectedLot(EOLotSalle selectedLot) {
		this.selectedLot = selectedLot;
		refreshChilds();
	}

	public EOLotSalle getSelectedLot() {
		return selectedLot;
	}

	public class DgDelegate {
		public void displayGroupDidChangeSelection(WODisplayGroup group) {
			setSelectedLot((EOLotSalle) group.selectedObject());
		}
	}

	public boolean canEditLots() {
		return ((Session) session()).gspotUser().canEditRefTable();
	}
	
	public boolean canAddChild() {
		return (selectedLot!=null)&&canEditLots();
	}

	public CktlAjaxWOComponent editMethodeObject() {
		return this;
	}

	public String auceditlotid() {
		return getComponentId() + "_auceditlotid";
	}

	public NSArray<String> getLstDelZones() {
		if (lstDelZones == null) {
			lstDelZones = new NSArray<String>(new String[] { auclisteid(),
					aucerreurid() });
		}
		return lstDelZones;
	}

	public NSArray<String> getLstDelSalleZones() {
		if (lstDelSalleZones == null) {
			lstDelSalleZones = new NSArray<String>(new String[] {
					lstSallesid(), aucerreurid() });
		}
		return lstDelSalleZones;
	}

	public NSArray<String> getLstDelIndZones() {
		if (lstDelIndZones == null) {
			lstDelIndZones = new NSArray<String>(new String[] { lstIndid(),
					aucerreurid() });
		}
		return lstDelIndZones;
	}

	public String aucerreurid() {
		return getComponentId() + "_aucerreurid";
	}

	public String idmessageutil() {
		return getComponentId() + "_idmessageutil";
	}

	public String deleteTrigId() {
		return getComponentId() + "_deleteTrigId";
	}

	private EOLotSalle editedLot;

	public void setEditedLot(EOLotSalle editedLot) {
		this.editedLot = editedLot;
	}

	public EOLotSalle editedLot() {
		return editedLot;
	}

	public WOActionResults addLot() {
		EOEditingContext ec = new EOEditingContext(session()
				.defaultEditingContext());
		setEditedLot(EOLotSalle.createFwkGspot_LotSalle(
				ec,
				null,
				(EOIndividu) EOIndividu.individuPourNumero(
						ec,
						((Session) session()).gspotUser().getUtilisateur()
								.noIndividu()).localInstanceIn(ec)));
		CktlAjaxWindow.open(context(), caweditlotid(), "Ajout d'un lot");
		return null;
	}

	public WOActionResults editLot(Object lot) {

		setEditedLot((EOLotSalle) lot);
		CktlAjaxWindow.open(context(), caweditlotid(), "Modification d'un lot");
		return null;
	}

	public void refreshChilds() {
		sallesForLot();
		indivForLot();
	}

	private WODisplayGroup dgSalles;

	public WODisplayGroup dgSalles() {
		if (dgSalles == null) {
			dgSalles = new WODisplayGroup();
			sallesForLot();
		}
		return dgSalles;
	}

	public WOActionResults sallesForLot() {
		if (selectedLot != null) {
			if (dgSalles == null)
				dgSalles = new WODisplayGroup();
			dgSalles.setObjectArray(EORepartLotSalle.fetchFwkGspot_RepartLotSalles(
					session().defaultEditingContext(),
					EOQualifier.qualifierWithQualifierFormat(
							EORepartLotSalle.TO_LOT_SALLE_KEY + "=%@",
							new NSArray<EOLotSalle>(getSelectedLot())),
					new NSArray<EOSortOrdering>(new EOSortOrdering(ERXQ
							.keyPath(EORepartLotSalle.TO_SALLES_KEY,
									EOSalles.SAL_PORTE_KEY),
							EOSortOrdering.CompareCaseInsensitiveAscending))));

		} else {
			dgSalles.setObjectArray(null);
		}
		dgSalles.clearSelection();
		return null;

	}

	public void setDgSalles(WODisplayGroup dgSalles) {
		this.dgSalles = dgSalles;
	}

	private WODisplayGroup dgIndiv;
	private EOSalles salleSearch;

	public WODisplayGroup dgIndiv() {
		if (dgIndiv == null) {
			dgIndiv = new WODisplayGroup();
			indivForLot();
		}
		return dgIndiv;
	}

	public WOActionResults indivForLot() {
		if (selectedLot != null) {
			if (dgIndiv == null)
				dgIndiv = new WODisplayGroup();
			dgIndiv.setObjectArray(EORepartLotIndividu.fetchFwkGspot_RepartLotIndividus(
					session().defaultEditingContext(),
					EOQualifier.qualifierWithQualifierFormat(
							EORepartLotIndividu.TO_LOT_SALLE_KEY + "=%@",
							new NSArray<EOLotSalle>(getSelectedLot())),
					new NSArray<EOSortOrdering>(
							new EOSortOrdering(
									ERXQ.keyPath(
											EORepartLotIndividu.TO_FWKPERS__INDIVIDU_KEY,
											EOIndividu.NOM_AFFICHAGE_KEY),
									EOSortOrdering.CompareCaseInsensitiveAscending))));

		} else {
			dgIndiv.setObjectArray(null);
		}
		dgIndiv.clearSelection();
		return null;

	}

	public void setDgIndiv(WODisplayGroup dgIndiv) {
		this.dgIndiv = dgIndiv;
	}

	public String aucchildssid() {
		return getComponentId() + "_aucchildssid";
	}

	public String buzzyid() {
		return getComponentId() + "_buzzyid";
	}

	public String lstSallesid() {
		return getComponentId() + "_lstSallesid";
	}

	public String lstIndid() {
		return getComponentId() + "_lstindid";
	}

	public String deleteSalleId() {
		return getComponentId() + "_deleteSalleId";
	}

	public String deleteIndId() {
		return getComponentId() + "_deleteIndId";
	}

	public WOActionResults addSalle() {
		CktlAjaxWindow.open(context(), cawaddsallelotid(),
				"Ajout de salles au lot");
		return null;
	}

	public String auceditSalleid() {
		return getComponentId() + "_auceditSalleid";
	}

	public WOActionResults addInd() {
		AjaxModalDialog.open(context(), camaddindivid());
		return null;
	}

	public String auceditIndid() {
		return getComponentId() + "_auceditIndid";
	}

	public WOActionResults editSalle(Object selSalle) {
		((Session) session()).setSelectedSalle(((EORepartLotSalle) selSalle)
				.toSalles());
		ERXRedirect redirect = new ERXRedirect(context());
		SalleGestion salleGest = (SalleGestion) pageWithName(SalleGestion.class
				.getName());
		salleGest.setEdited(false);
		((Session) session()).setSelectedOnglet(TabInfosSalles.class.getName());
		redirect.setComponent(salleGest);
		return redirect;
	}

	/**
	 * @return the salleSearch
	 */
	public EOSalles salleSearch() {
		return salleSearch;
	}

	/**
	 * @param salleSearch
	 *            the salleSearch to set
	 */
	public void setSalleSearch(EOSalles salleSearch) {
		this.salleSearch = salleSearch;
	}

	public String aucsallesearchid() {
		return getComponentId() + "_aucsallesearchid";
	}

	public WOActionResults searchSalle() {
		if (!searchVisible()) {
			setSearchVisible(true);
			// minimizedSearch=Boolean.FALSE;
			openWinSearch();
		}
		return null;
	}

	public WOActionResults openWinSearch() {
		if (searchVisible()) {
			CktlAjaxWindow.open(context(), cawSearchSalleid(),
					"Recherche de salle");
		}
		return null;
	}

	public WOActionResults closeSearch() {
		setSearchVisible(false);
		return null;
	}

	private Boolean searchVisible = Boolean.FALSE;

	/**
	 * @return the searchVisible
	 */
	public boolean searchVisible() {
		return searchVisible;
	}

	/**
	 * @param searchVisible
	 *            the searchVisible to set
	 */
	public void setSearchVisible(boolean searchVisible) {
		this.searchVisible = searchVisible;

	}

	private SallesTreeDelegate treeDelegate;
	private EOIndividu indivSearch;
	private boolean isWindowIndVisible;
	private EOIndividu selectedSearchPersonne;

	public SallesTreeDelegate getTreeDelegate() {

		if ((treeDelegate != null)
				&& (treeDelegate instanceof SallesTreeDelegate)) {

		} else {
			treeDelegate = new SallesTreeDelegate(session()
					.defaultEditingContext());

		}

		return treeDelegate;
	}

	public void setTreeDelegate(SallesTreeDelegate treeDelegate) {
		this.treeDelegate = treeDelegate;
	}

	public String aucsearchedsalleid() {
		return getComponentId() + "_aucsearchedsalleid";
	}

	public String divSearchId() {
		return getComponentId() + "_divSearchId";
	}

	public String cawSearchSalleid() {
		return getComponentId() + "_cawSearchSalleid";
	}

	public WOActionResults resetSearchSalle() {
		setSalleSearch(null);
		return null;
	}

	public String aucsearchedindivid() {
		return getComponentId() + "_aucsearchedindivid";
	}

	/**
	 * @return the indivSearch
	 */
	public EOIndividu indivSearch() {
		return indivSearch;
	}

	/**
	 * @param indivSearch
	 *            the indivSearch to set
	 */
	public void setIndivSearch(EOIndividu indivSearch) {
		this.indivSearch = indivSearch;
	}

	public String aucindivsearchid() {
		return getComponentId() + "_aucindivsearchid";
	}

	public WOActionResults searchIndiv() {
		setWindowIndVisible(true);
		AjaxModalDialog.open(context(), camsearchyindid());
		return null;
	}

	public String camsearchyindid() {
		return getComponentId() + "_camsearchyindid";
	}

	/**
	 * @return the isWindowIndVisible
	 */
	public boolean isWindowIndVisible() {
		return isWindowIndVisible;
	}

	/**
	 * @param isWindowIndVisible
	 *            the isWindowIndVisible to set
	 */
	public void setWindowIndVisible(boolean isWindowIndVisible) {
		this.isWindowIndVisible = isWindowIndVisible;
	}

	/**
	 * @return the selectedSearchPersonne
	 */
	public EOIndividu selectedSearchPersonne() {
		return selectedSearchPersonne;
	}

	/**
	 * @param selectedSearchPersonne
	 *            the selectedSearchPersonne to set
	 */
	public void setSelectedSearchPersonne(EOIndividu selectedSearchPersonne) {
		this.selectedSearchPersonne = selectedSearchPersonne;
	}

	private String searchTypeIntExt = "interne";
	private boolean isWindowOwnerVisible;
	private EOIndividu selectedSearchOwner;
	private EOIndividu ownerSearch;
	private EOSalles addSalleSearch;
	private Boolean searchAddSalleVisible = Boolean.FALSE;
	private EOIndividu selectedAddIndiv;

	/**
	 * @return the searchTypeIntExt
	 */
	public String searchTypeIntExt() {
		return searchTypeIntExt;
	}

	/**
	 * @param searchTypeIntExt
	 *            the searchTypeIntExt to set
	 */
	public void setSearchTypeIntExt(String searchTypeIntExt) {
		this.searchTypeIntExt = searchTypeIntExt;
	}

	public String aucselpersonneid() {
		return getComponentId() + "_aucselpersonneid";
	}

	public WOActionResults chooseSearchInd() {
		setIndivSearch(selectedSearchPersonne);
		closeSearchInd();
		return null;
	}

	public WOActionResults closeSearchInd() {
		setWindowIndVisible(false);
		AjaxModalDialog.close(context());
		AjaxUpdateContainer.updateContainerWithID(aucsearchedindivid(),
				context());
		return null;
	}

	public WOActionResults resetSearchIndiv() {
		setIndivSearch(null);
		return null;
	}

	public String listeVideMesage() {
		if ((displayGroup().allObjects() != null)
				&& (displayGroup().allObjects().size() > 0)) {
			if (selectedLot != null) {
				return "Aucun élément pour ce lot";
			} else {
				return "Choisissez un lot";
			}
		}
		return "...";
	}

	public String aucownersearchid() {
		return getComponentId() + "_aucownersearchid";
	}

	public String camsearchyownerid() {
		return getComponentId() + "_camsearchyownerid";
	}

	/**
	 * @return the isWindowOwnerVisible
	 */
	public boolean isWindowOwnerVisible() {
		return isWindowOwnerVisible;
	}

	/**
	 * @param isWindowOwnerVisible
	 *            the isWindowOwnerVisible to set
	 */
	public void setWindowOwnerVisible(boolean isWindowOwnerVisible) {
		this.isWindowOwnerVisible = isWindowOwnerVisible;
	}

	/**
	 * @return the selectedSearchOwner
	 */
	public EOIndividu selectedSearchOwner() {
		return selectedSearchOwner;
	}

	/**
	 * @param selectedSearchOwner
	 *            the selectedSearchOwner to set
	 */
	public void setSelectedSearchOwner(EOIndividu selectedSearchOwner) {
		this.selectedSearchOwner = selectedSearchOwner;
	}

	public String aucselownerid() {
		return getComponentId() + "_aucselownerid";
	}

	public WOActionResults chooseSearchOwner() {
		setOwnerSearch(selectedSearchOwner);
		closeSearchOwner();
		return null;
	}

	public WOActionResults closeSearchOwner() {
		setWindowOwnerVisible(false);
		AjaxModalDialog.close(context());
		AjaxUpdateContainer.updateContainerWithID(aucsearchedownerid(),
				context());
		return null;
	}

	public WOActionResults resetSearchOwner() {
		setOwnerSearch(null);
		return null;
	}

	/**
	 * @return the ownerSearch
	 */
	public EOIndividu ownerSearch() {
		return ownerSearch;
	}

	/**
	 * @param ownerSearch
	 *            the ownerSearch to set
	 */
	public void setOwnerSearch(EOIndividu ownerSearch) {
		this.ownerSearch = ownerSearch;
	}

	public String aucsearchedownerid() {
		return getComponentId() + "_aucsearchedownerid";
	}

	public WOActionResults searchOwner() {
		setWindowOwnerVisible(true);
		AjaxModalDialog.open(context(), camsearchyownerid());
		return null;
	}

	public String caweditlotid() {
		return getComponentId() + "_caweditlotid";
	}

	public WOActionResults validAddLot() {
		try {
			editedLot.validateForSave();

		} catch (ValidationException e) {
			UtilMessages.creatMessageUtil(session(),
					UtilMessages.ERROR_MESSAGE, e.getMessage());
			AjaxUpdateContainer.updateContainerWithID(aucerreurlotid(),
					context());
			return null;
		}
		int mode = -1;
		if (editedLot().editingContext().insertedObjects().size() > 0)
			mode = 0;
		else
			mode = 1;
		editedLot.editingContext().saveChanges();
		if (editedLot.editingContext().parentObjectStore()
				.equals(session().defaultEditingContext()))
			session().defaultEditingContext().saveChanges();

		if (mode == 0) {
			setSearchLib(editedLot().lotLibelle());
			searchObj();
			UtilMessages.creatMessageUtil(session(), UtilMessages.INFO_MESSAGE,
					"Lot créé ");
			EOEditingContext ec = new EOEditingContext(session()
					.defaultEditingContext());
			setEditedLot(EOLotSalle.createFwkGspot_LotSalle(ec, null,
					editedLot().toFwkpers_Individu().localInstanceIn(ec)));
			AjaxUpdateContainer.updateContainerWithID(aucerreurlotid(),
					context());
			AjaxUpdateContainer.updateContainerWithID(auceditlotinsideid(),
					context());

		} else {
			CktlAjaxWindow.close(context(), caweditlotid());
		}

		return null;
	}

	public String aucerreurlotid() {
		return getComponentId() + "_aucerreurlotid";
	}

	public String idmessageutillot() {
		return getComponentId() + "_idmessageutillot";
	}

	public WOActionResults cancel() {
		editedLot().editingContext().revert();
		CktlAjaxWindow.close(context(), caweditlotid());
		return null;
	}

	public String auceditlotinsideid() {
		return getComponentId() + "_auceditlotinsideid";
	}

	public String cawaddsallelotid() {
		return getComponentId() + "_cawaddsallelotid";
	}

	/**
	 * @return the addSalleSearch
	 */
	public EOSalles addSalleSearch() {
		return addSalleSearch;
	}

	/**
	 * @param addSalleSearch
	 *            the addSalleSearch to set
	 */
	public void setAddSalleSearch(EOSalles addSalleSearch) {
		this.addSalleSearch = addSalleSearch;
	}

	public String divAddSearchId() {
		return getComponentId() + "_divAddSearchId";
	}

	public String aucseladdsalleid() {
		return getComponentId() + "_aucseladdsalleid";
	}

	/**
	 * @return the searchAddSalleVisible
	 */
	public Boolean searchAddSalleVisible() {
		return searchAddSalleVisible;
	}

	/**
	 * @param searchAddSalleVisible
	 *            the searchAddSalleVisible to set
	 */
	public void setSearchAddSalleVisible(Boolean searchAddSalleVisible) {
		this.searchAddSalleVisible = searchAddSalleVisible;
	}

	public boolean isSalleInLot() {
		for (Object repart : dgSalles().allObjects()) {
			if (((EORepartLotSalle) repart).toSalles()
					.localInstanceIn(addSalleSearch().editingContext())
					.equals(addSalleSearch()))
				return true;
		}
		return false;
	}

	public WOActionResults validAjoutSalle() {
		if (addSalleSearch() != null) {
			EORepartLotSalle newRep = EORepartLotSalle
					.createFwkGspot_RepartLotSalle(
							addSalleSearch.editingContext(), selectedLot,
							addSalleSearch());
			newRep.editingContext().saveChanges();
			sallesForLot();
			AjaxUpdateContainer.updateContainerWithID(lstSallesid(), context());
		}
		return null;
	}

	public String aucaddindivid() {
		return getComponentId() + "_aucaddindivid";
	}

	public String camaddindivid() {
		return getComponentId() + "_camaddindivid";
	}

	/**
	 * @return the selectedAddIndiv
	 */
	public EOIndividu selectedAddIndiv() {
		return selectedAddIndiv;
	}

	/**
	 * @param selectedAddIndiv
	 *            the selectedAddIndiv to set
	 */
	public void setSelectedAddIndiv(EOIndividu selectedAddIndiv) {
		this.selectedAddIndiv = selectedAddIndiv;
	}

	public String aucseladdindivid() {
		return getComponentId() + "_aucseladdindivid";
	}

	public boolean isSelAddIndivInLot() {
		for (Object repart : dgIndiv().allObjects()) {
			if (((EORepartLotIndividu) repart).toFwkpers_Individu().persId()
					.equals(selectedAddIndiv().persId()))
				return true;
		}
		return false;
	}

	public WOActionResults validAjoutIndiv() {
		if (selectedAddIndiv()!=null){
			EORepartLotIndividu newRep = EORepartLotIndividu.createFwkGspot_RepartLotIndividu(selectedAddIndiv().editingContext(), selectedAddIndiv(),selectedLot);
			newRep.editingContext().saveChanges();
			indivForLot();
			AjaxUpdateContainer.updateContainerWithID(lstIndid(), context());
		}
		return null;
	}

	public WOActionResults closeAddIndiv() {
		AjaxModalDialog.close(context());
		return null;
	}
}