package org.cocktail.gspot.serveur;

import org.cocktail.fwkcktldroitsutils.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.PersonneApplicationUser;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametresType;
import org.cocktail.fwkcktlwebapp.server.CktlParamManager;

import com.webobjects.eoaccess.EOEntity;
import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXEOAccessUtilities;
import er.extensions.foundation.ERXThreadStorage;

public class GspotParamManager extends CktlParamManager {

	public static final String GSPOT_CSS = "org.cocktail.gspot.css";
	
	private EOEditingContext ec = ERXEC.newEditingContext();
	
	public GspotParamManager() {
		
		getParamList().add(GSPOT_CSS);
		getParamComments().put(GSPOT_CSS, "Choix du CSS d'habillage en Orange, Vert ou Bleu (par défaut : CktlCommonOrange.css ");
		getParamDefault().put(GSPOT_CSS, "CktlCommonOrange.css");
		getParamTypes().put(GSPOT_CSS, EOGrhumParametresType.texteLibre);
		
	}
	
	@Override
	public void createNewParam(String key, String value, String comment) {
		createNewParam(key, value, comment, EOGrhumParametresType.codeActivation);
	}
	
	@Override
	public void checkAndInitParamsWithDefault() {
		//Recuperer un grhum_createur
		String cptLogin = EOGrhumParametres.parametrePourCle(ec, EOGrhumParametres.PARAM_GRHUM_CREATEUR);
		if (cptLogin != null) {
			EOCompte cpt = EOCompte.compteForLogin(ec, cptLogin);
			if (cpt != null) {
				ERXThreadStorage.takeValueForKey(cpt.persId(), PersonneApplicationUser.PERS_ID_CURRENT_USER_STORAGE_KEY);
			}
		}
		super.checkAndInitParamsWithDefault();
	}

	@Override
	public void createNewParam(String key, String value, String comment,
			String type) {
		EOGrhumParametres newParametre = EOGrhumParametres.creerInstance(ec);
		newParametre.setParamKey(key);
		newParametre.setParamValue(value);
		newParametre.setParamCommentaires(comment);
		newParametre.setToParametresTypeRelationship(EOGrhumParametresType.fetchByKeyValue(ec, EOGrhumParametresType.TYPE_ID_INTERNE_KEY, type));
		if (ec.hasChanges()) {
			EOEntity entityParameter = ERXEOAccessUtilities.entityForEo(newParametre);
			try {

				// Avant de sauvegarder les données, nous modifions le modèle
				// pour que l'on puisse avoir accès aussi en écriture sur les données
				entityParameter.setReadOnly(false);
				ec.saveChanges();

			} catch (Exception e) {
				log.warn("Erreur lors de l'enregistrement des parametres.");
				e.printStackTrace();
			} finally {
				entityParameter.setReadOnly(true);
			}
		}
	}

	@Override
	public String getParam(String key) {
		String res = getApplication().config().stringForKey(key);
		//		String res = EOGrhumParametres.parametrePourCle(ec, key);
		return res;
	}

}
